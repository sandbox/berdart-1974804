<?php

/**
 * Implements hook_rules_condition_info().
 */
function sites_rules_condition_info() {
  $conditions = array();
  $conditions['sites_rules_condition_current_site'] = array(
    'label' => t('Site name'),
    'parameter' => array(
      'sites' => array(
        'type' => 'token',
        'label' => t('Select site'),
        'options list' => 'sites_rules_sites_options',
        'restriction' => 'input',
      ),
    ),
    'group' => t('Sites'),
  );
  return $conditions;
}

/**
 * Callback function for sites_rules_condition_info().
 */
function sites_rules_condition_current_site($sites, $settings) {
  $site = SiteController::getCurrentSite();
  return ($site->sid === $sites);
}

/**
 * Callback function for returning all configured sites.
 */
function sites_rules_sites_options() {
  $options = array();
  $sites = SiteController::getSites();
  foreach($sites as $site) {
    $options[$site->sid] = $site->name;
  }
  return $options;
}
